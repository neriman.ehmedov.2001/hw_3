package com.company;
import java.util.Scanner;

public class HW3 {
    public static void main(String[] args) {
        String[][] schedule = new String[7][2];

        schedule[0][0] = "Sunday";      schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";      schedule[1][1] = "got to work and then, watch a film";
        schedule[2][0] = "Tuesday";     schedule[2][1] = "go to IBA Tech then watch new episode of animes";
        schedule[3][0] = "Wednesday";   schedule[3][1] = "do course`s homeworks; go to work";
        schedule[4][0] = "Thursday";    schedule[4][1] = "Join online class; go to IBA";
        schedule[5][0] = "Friday";      schedule[5][1] = "join online lesson; watch anime";
        schedule[6][0] = "Saturday";    schedule[6][1] = "join online IBA then relax the rest of day";

        Scanner scan = new Scanner(System.in);
        String user_day = "#";

        loop: while(user_day != "exit") {
            System.out.print("Please, input the day of the week: ");
            user_day = (scan.next()).toLowerCase();

            switch(user_day) {
                case "sunday":
                    System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1] + ".");
                    break;

                case "monday":
                    System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1] + ".");
                    break;

                case "tuesday":
                    System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1] + ".");
                    break;

                case "wednesday":
                    System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1] + ".");
                    break;

                case "thursday":
                    System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1] + ".");
                    break;

                case "friday":
                    System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1] + ".");
                    break;

                case "saturday":
                    System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1] + ".");
                    break;

                case "exit":
                    break loop;

                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }
}
